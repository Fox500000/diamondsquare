#version 400 core
layout (location = 0) in vec3 vertexPosition;
uniform float aspectRatio;
uniform mat4 mvp;

void main()
{
    gl_Position = vec4(vertexPosition, 1.0);
}
