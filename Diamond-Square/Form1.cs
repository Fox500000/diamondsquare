﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Forms;
using GlmNet;
using System.ComponentModel;
using System.Threading;
using System.Runtime.InteropServices;

namespace Diamond_Square
{
    public partial class Form1 : Form
    {
        Controls ControlHandler;
        Algorithm AlgorithmHandler;
        OBJExport Exporter;

        OpenGL GL;
        Shader LandscapeShader;

        vec3 CameraPos = new vec3(1);
        vec3 LookVector = new vec3(0);

        mat4 ModelMatrix = mat4.identity();

        float AspectRatio;
        int DrawRange = 700;

        bool DataTaken = false;

        uint[] EBO = new uint[1] { 0 };
        uint[] VBO = new uint[1] { 0 };
        uint[] VAO = new uint[1] { 0 };

        int NumIndexes = 0;

        public Form1(Controls control, Algorithm algorithm, OBJExport export)
        {
            InitializeComponent();
            ControlHandler = control;
            AlgorithmHandler = algorithm;
            Exporter = export;
            GL = this.openGLControl.OpenGL;
            AspectRatio = (float)openGLControl.Width / (float)openGLControl.Height;

            LandscapeShader = new Shader("vertexshader.vert", "fragmentshader.frag", GL, "geometryshader.geom");
            GL.GenVertexArrays(1, VAO);
            GL.GenBuffers(1, VBO);
            GL.GenBuffers(1, EBO);

            GL.BindVertexArray(VAO[0]);
            GL.BindBuffer(OpenGL.GL_ARRAY_BUFFER, VBO[0]);
            GL.BindBuffer(OpenGL.GL_ELEMENT_ARRAY_BUFFER, EBO[0]);
            GL.VertexAttribPointer(0, 3, OpenGL.GL_FLOAT, false, 0, IntPtr.Zero);
            GL.EnableVertexAttribArray(0);

            GL.BindVertexArray(0);
            GL.BindBuffer(OpenGL.GL_ARRAY_BUFFER, 0);
            GL.BindBuffer(OpenGL.GL_ELEMENT_ARRAY_BUFFER, 0);
            GL.UseProgram(LandscapeShader.ID);
            GL.Uniform1(GL.GetUniformLocation(LandscapeShader.ID, "aspectRatio"), AspectRatio);
            GL.Uniform1(GL.GetUniformLocation(LandscapeShader.ID, "dimension"), AlgorithmHandler.GetDimension());
            GL.Uniform1(GL.GetUniformLocation(LandscapeShader.ID, "drawDistance"), (float)DrawRange);
            GL.Uniform1(GL.GetUniformLocation(LandscapeShader.ID, "height"), (float)AlgorithmHandler.GetHeight());
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void OpenGLDraw(object sender, RenderEventArgs args)
        {
            GL.Enable(OpenGL.GL_CCW);
            GL.Enable(OpenGL.GL_DEPTH_TEST);
            GL.Enable(OpenGL.GL_DEPTH_BUFFER);
            GL.Enable(OpenGL.GL_MULTISAMPLE);

            GL.ClearColor(0.1f, 0.1f, 0.1f, 1.0f);
            GL.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);

            GL.UseProgram(LandscapeShader.ID);
            GL.Uniform3(GL.GetUniformLocation(LandscapeShader.ID, "camera"), 1, ControlHandler.CameraPos.to_array());

            mat4 ProjectionMatrix = glm.perspective(glm.radians(51.0f), AspectRatio, 0.1f, DrawRange);
            mat4 ViewMatrix = glm.lookAt(ControlHandler.CameraPos, ControlHandler.CameraPos + ControlHandler.LookVector, new vec3(0.0f, 0.0f, 1.0f));

            GL.UniformMatrix4(GL.GetUniformLocation(LandscapeShader.ID, "model"), 1, false, ModelMatrix.to_array());
            GL.UniformMatrix4(GL.GetUniformLocation(LandscapeShader.ID, "view"), 1, false, ViewMatrix.to_array());
            GL.UniformMatrix4(GL.GetUniformLocation(LandscapeShader.ID, "projection"), 1, false, ProjectionMatrix.to_array());

            DrawArray();
        }

        void DrawArray()
        {
            if (!DataTaken)
            {
                uint[] Indexes;
                float[] DrawingData;
                (DrawingData, Indexes) = GetArrayForDrawing(AlgorithmHandler.GetArray(), AlgorithmHandler.GetDimension(), false);
                NumIndexes = Indexes.Length;
                GL.BindVertexArray(VAO[0]);
                GL.BindBuffer(OpenGL.GL_ARRAY_BUFFER, VBO[0]);
                GL.BufferData(OpenGL.GL_ARRAY_BUFFER, DrawingData, OpenGL.GL_STATIC_DRAW);
                GL.BindBuffer(OpenGL.GL_ELEMENT_ARRAY_BUFFER, EBO[0]);
                unsafe
                {
                    fixed (uint* pointer = Indexes)
                    {
                        IntPtr actualP = (IntPtr)pointer;
                        GL.BufferData(OpenGL.GL_ELEMENT_ARRAY_BUFFER, Indexes.Length * sizeof(uint), actualP, OpenGL.GL_STATIC_DRAW);
                    }
                }
                GL.BindVertexArray(0);
                GL.BindBuffer(OpenGL.GL_ARRAY_BUFFER, 0);
                GL.BindBuffer(OpenGL.GL_ELEMENT_ARRAY_BUFFER, 0);
                DataTaken = true;
            }
            GL.BindVertexArray(VAO[0]);
            GL.DrawElements(OpenGL.GL_TRIANGLE_STRIP, NumIndexes, OpenGL.GL_UNSIGNED_INT, IntPtr.Zero);
            GL.BindVertexArray(0);
        }

        private void KeyboardPress(object sender, KeyPressEventArgs e)
        {
            ControlHandler.KeyboardPressHandle(sender, e);
        }

        private void openGLControl_Load(object sender, EventArgs e)
        {

        }

        private float GetDistance(float[] vector1, float[] vector2)
        {
            float x = vector2[0] - vector1[0];
            float y = vector2[1] - vector1[1];
            float z = vector2[2] - vector1[2];
            return x * x + y * y + z * z;
        }

        (float[], uint[]) GetArrayForDrawing(float[,] arr, uint dim, bool lowRes)
        {
            List<float> Result = new List<float>();
            List<uint> Indexes = new List<uint>();
            int Resolution = lowRes ? 2 : 1;
            for (uint i = 0; i < dim; i++)
            {
                for (uint j = 0; j < dim; j++)
                {
                    Result.Add((float)i * 1000 / dim);
                    Result.Add((float)j * 1000 / dim);
                    Result.Add(arr[i, j]);
                }
            }

            for (uint i = 0; i < dim - 1; i++)
            {
                Indexes.Add(i * dim);
                Indexes.Add((i + 1) * dim);
                for (uint j = 1; j < dim; j++)
                {
                    Indexes.Add(i * dim + j);
                    Indexes.Add((i + 1) * dim + j);
                }
                Indexes.Add((i + 1) * dim + (dim - 1));
                Indexes.Add((i + 1) * dim);
            }
            return (Result.ToArray(), Indexes.ToArray());
        }
        private void EnableUI(bool active)
        {
            MapSizeTrackBar.Enabled = 
            ErosionIterationsNum.Enabled = 
            UseSeedCheckBox.Enabled = 
            SeedNumeric.Enabled = 
            EvapRateNum.Enabled = 
            ParticleVolumeNum.Enabled = 
            MinVolumeNum.Enabled = 
            DensityNum.Enabled = 
            FrictionNum.Enabled = 
            DepositionRateNum.Enabled = 
            TimeStepNum.Enabled = 
            GeneratorButton.Enabled = 
            ExportButton.Enabled = 
            ParallelErosionCheck.Enabled = 
            ParticlePerCycleNum.Enabled = active;

        }
        private void GeneratorButton_Click(object sender, EventArgs e)
        {
            EnableUI(false);
            backgroundWorker1.RunWorkerAsync();
        }

        private void ControlsInfo_Enter(object sender, EventArgs e)
        {
            ActiveControl = openGLControl;
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            AlgorithmHandler.SetupArray(backgroundWorker1);
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar.Value = e.ProgressPercentage;
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            DataTaken = false;
            EnableUI(true);
            openGLControl.Focus();

        }

        private void MapSizeTrackBar_ValueChanged(object sender, EventArgs e)
        {
            AlgorithmHandler.ChangeMapSize(MapSizeTrackBar.Value);
            MapSizeInfo.Text = (Convert.ToUInt32(Math.Pow(2, MapSizeTrackBar.Value)) + 1).ToString();
        }

        private void ErosionIterationsNum_ValueChanged(object sender, EventArgs e)
        {
            AlgorithmHandler.ChangeErosionIterationsCount((int)ErosionIterationsNum.Value);
        }

        private void UseSeedCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            bool checkedState = UseSeedCheckBox.Checked;
            SeedLabel.Enabled = checkedState;
            SeedNumeric.Enabled = checkedState;
            AlgorithmHandler.EnableSeed(checkedState);
            AlgorithmHandler.ChangeSeed((int)SeedNumeric.Value);
        }

        private void SeedNumeric_ValueChanged(object sender, EventArgs e)
        {
            AlgorithmHandler.ChangeSeed((int)SeedNumeric.Value);
        }

        private void EvapRateNum_ValueChanged(object sender, EventArgs e)
        {
            AlgorithmHandler.ChangeEvapRate((float)EvapRateNum.Value);
        }

        private void ParticleVolumeNum_ValueChanged(object sender, EventArgs e)
        {
            AlgorithmHandler.ChangeVolume((float)ParticleVolumeNum.Value);
        }

        private void MinVolumeNum_ValueChanged(object sender, EventArgs e)
        {
            AlgorithmHandler.ChangeMinVolume((float)MinVolumeNum.Value);
        }

        private void DensityNum_ValueChanged(object sender, EventArgs e)
        {
            AlgorithmHandler.ChangeDensity((float)DensityNum.Value);
        }

        private void FrictionNum_ValueChanged(object sender, EventArgs e)
        {
            AlgorithmHandler.ChangeFriction((float)FrictionNum.Value);
        }

        private void DepositionRateNum_ValueChanged(object sender, EventArgs e)
        {
            AlgorithmHandler.ChangeDepositionRate((float)DepositionRateNum.Value);
        }

        private void TimeStepNum_ValueChanged(object sender, EventArgs e)
        {
            AlgorithmHandler.ChangeTimestep((float)TimeStepNum.Value);
        }

        private void ParticlePerCycleNum_ValueChanged(object sender, EventArgs e)
        {
            AlgorithmHandler.ChangeParticlePerCycleCount((uint)ParticlePerCycleNum.Value);
        }

        private void ExportButton_Click(object sender, EventArgs e)
        {
            EnableUI(false);
            backgroundWorkerExport.RunWorkerAsync();
        }

        private void backgroundWorkerExport_DoWork(object sender, DoWorkEventArgs e)
        {
            var data = AlgorithmHandler.GetArray();
            var dim = AlgorithmHandler.GetDimension();
            Exporter.Export(data, dim, backgroundWorkerExport);
        }

        private void backgroundWorkerExport_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            EnableUI(true);
            openGLControl.Focus();
        }

        private void ParallelErosionCheck_CheckedChanged(object sender, EventArgs e)
        {
            AlgorithmHandler.ParallelErosion(ParallelErosionCheck.Checked);
        }

        private void SmoothingCheck_CheckedChanged(object sender, EventArgs e)
        {
            AlgorithmHandler.EnableSmoothing(SmoothingCheck.Checked);
        }
    }
}
