﻿using System;
using System.Collections.Generic;
using GlmNet;
using System.Windows.Forms;
using System.ComponentModel;
using System.Threading.Tasks;

namespace Diamond_Square
{
    public static class MissingMath
    {
        public static float Length(vec2 vec)
        {
            return (float)Math.Sqrt(Math.Pow(vec.x, 2) + Math.Pow(vec.y, 2));
        }
    }
    public class Algorithm
    {
        //Particle settings
        float EvaporationRate = 0.01f;
        float ParticleVolume = 1.0f;
        float MinVolume = 0.01f;
        float Density = 1.0f;
        float Friction = 0.05f;
        float DepositionRate = 0.2f;
        float dt = 1.0f;

        uint ParticlePerCycleCount = 10000;

        float[,] Data;
        bool[,] VertexPlacement;
        uint Dimension;
        int Height;
        int progress;
        int ErosionIterations = 0;
        int Seed;
        bool working = false;
        bool SeedIsUsed = false;
        bool Smoothing = true;
        bool parallel = true;
        public Algorithm()
        {
            Dimension = 129;
            Height = 100;
            //worker.WorkerReportsProgress = true;
            Data = new float[Dimension, Dimension];
            //SetupArray();
        }
        public void EnableSmoothing(bool val) => Smoothing = val;
        public void ParallelErosion(bool val) => parallel = val;
        public void ChangeEvapRate(float newVal) => EvaporationRate = newVal <= 0 ? 0.0001f : newVal;
        public void ChangeVolume(float newVal) => ParticleVolume = newVal <= 0 ? 0.0001f : newVal;
        public void ChangeMinVolume(float newVal) => MinVolume = newVal <= 0 ? 0.0001f : newVal;
        public void ChangeDensity(float newVal) => Density = newVal <= 0 ? 0.0001f : newVal;
        public void ChangeFriction(float newVal) => Friction = newVal <= 0 ? 0.0001f : newVal;
        public void ChangeDepositionRate(float newVal) => DepositionRate = newVal <= 0 ? 0.0001f : newVal;
        public void ChangeTimestep(float newVal) => dt = newVal <= 0 ? 0.0001f : newVal;

        public void ChangeParticlePerCycleCount(uint newVal) => ParticlePerCycleCount = newVal < 0 ? 0 : newVal;

        public void EnableSeed(bool active) => SeedIsUsed = active;

        public void ChangeSeed(int seed) => Seed = seed;

        public void ChangeErosionIterationsCount(int val)
        {
            ErosionIterations = val;
        }
        public void ChangeMapSize(int dimRes)
        {
            if (!working)
            {
                Dimension = Convert.ToUInt32(Math.Pow(2, dimRes)) + 1;
                //Height = Convert.ToInt32(Dimension / 6);
            }
        }
        public void Clear()
        {
            Data = null;
            VertexPlacement = null;
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }
        public void SetupArray(BackgroundWorker worker)
        {
            working = true;
            progress = 0;
            GC.Collect();
            Data = new float[Dimension, Dimension];
            VertexPlacement = new bool[Dimension, Dimension];
            //Erosion = new float[Dimension, Dimension];
            //RandomGen();
            DiamondSquare(Height);
            progress = 10;
            worker.ReportProgress(progress);
            for (int i = 0; i < ErosionIterations; i++)
            {
                UpdatedErosion(ParticlePerCycleCount, i, parallel);
                progress += (int)(90.0f / ErosionIterations);
                worker.ReportProgress(progress);
            }
            worker.ReportProgress(100);
            GC.Collect();
            working = false;
        }

        void RandomGen()
        {
            Random Rand = new Random();
            for (int i = 0; i < Dimension; i++)
            {
                for (int k = 0; k < Dimension; k++)
                {
                    Data[i, k] = (float)Rand.NextDouble() * Height * 2 - Height * 0.3f;
                }
            }
            for (int SideLength = Convert.ToInt32(Dimension - 1); SideLength >= 2; SideLength /= 2)
            {
                int half = SideLength / 2;
                for (int x = 0; x < Dimension; x += half)
                {
                    for (int y = 0; y < Dimension; y += half)
                    {
                        int exposure = half > 64 ? 64 : half > 16 ? 10 : half > 8 ? 6 : half > 4 ? 2 : 1;
                        float Left = (y - exposure) < 0 ? Data[x, y] : Data[x, y - exposure];
                        float Right = (y + exposure) > (Dimension - exposure) ? Data[x, y] : Data[x, y + exposure];
                        float Bottom = (x + exposure) > (Dimension - exposure) ? Data[x, y] : Data[x + exposure, y];
                        float Top = (x - exposure) < 0 ? Data[x, y] : Data[x - exposure, y];
                        float average = (Left + Right + Top + Bottom) / 4;
                        if (Math.Pow(Math.Abs(average - Data[x, y]), 1.05) > 0.25f) Data[x, y] = average;
                    }
                }
            }
        }

        void DiamondSquare(float roughness)
        {
            float RandomOffset = roughness;
            Random rand;

            if (SeedIsUsed) rand = new Random(Seed);
            else rand = new Random();

            Data[0, 0] = (float)rand.NextDouble() * 2 * RandomOffset - RandomOffset * 0.7f;
            Data[0, Dimension - 1] = (float)rand.NextDouble() * 2 * RandomOffset - RandomOffset * 0.7f;
            Data[Dimension - 1, 0] = (float)rand.NextDouble() * 2 * RandomOffset - RandomOffset * 0.7f;
            Data[Dimension - 1, Dimension - 1] = (float)rand.NextDouble() * 2 * RandomOffset - RandomOffset * 0.7f;
            for (int SideLength = Convert.ToInt32(Dimension - 1); SideLength >= 2; SideLength = SideLength / 2, RandomOffset /= 1.9f)
            {
                int middlePoint = SideLength / 2;

                //Diamond
                for (int x = 0; x < Dimension - 1; x += SideLength)
                {
                    for (int y = 0; y < Dimension - 1; y += SideLength)
                    {

                        float average = (Data[x, y] + Data[x + SideLength, y] + Data[x, y + SideLength] + Data[x + SideLength, y + SideLength]) / 4;
                        if (!VertexPlacement[x + middlePoint, y + middlePoint])
                        {
                            Data[x + middlePoint, y + middlePoint] = average + ((float)rand.NextDouble() * 2 * RandomOffset) - RandomOffset;
                            VertexPlacement[x + middlePoint, y + middlePoint] = true;
                        }
                    }
                }

                //Square
                for (int x = 0; x < Dimension - 1; x += SideLength)
                {
                    for (int y = 0; y < Dimension - 1; y += SideLength)
                    {
                        if (!VertexPlacement[x + middlePoint, y])
                        {
                            float averageLeft = y - middlePoint < 0 ?
                            (Data[x, y] + Data[x + SideLength, y] + Data[x + middlePoint, y + middlePoint]) / 3 :
                            (Data[x, y] + Data[x + SideLength, y] + Data[x + middlePoint, y + middlePoint] + Data[x + middlePoint, y - middlePoint]) / 4;
                            averageLeft += (float)rand.NextDouble() * 2 * RandomOffset - RandomOffset;

                            Data[x + middlePoint, y] = averageLeft;
                            VertexPlacement[x + middlePoint, y] = true;
                        }
                        if (!VertexPlacement[x, y + middlePoint])
                        {
                            float averageTop = x - middlePoint < 0 ?
                            (Data[x, y] + Data[x, y + SideLength] + Data[x + middlePoint, y + middlePoint]) / 3 :
                            (Data[x, y] + Data[x, y + SideLength] + Data[x + middlePoint, y + middlePoint] + Data[x - middlePoint, y + middlePoint]) / 4;
                            averageTop += (float)rand.NextDouble() * 2 * RandomOffset - RandomOffset;

                            Data[x, y + middlePoint] = averageTop;
                            VertexPlacement[x, y + middlePoint] = true;
                        }
                        if (!VertexPlacement[x + SideLength, y + middlePoint])
                        {
                            float averageBottom = x + SideLength + middlePoint > Dimension - 1 ?
                            (Data[x + SideLength, y] + Data[x + SideLength, y + SideLength] + Data[x + middlePoint, y + middlePoint]) / 3 :
                            (Data[x + SideLength, y] + Data[x + SideLength, y + SideLength] + Data[x + middlePoint, y + middlePoint] +
                            Data[x + SideLength + middlePoint, y + middlePoint]) / 4;
                            averageBottom += (float)rand.NextDouble() * 2 * RandomOffset - RandomOffset;

                            Data[x + SideLength, y + middlePoint] = averageBottom;
                            VertexPlacement[x + SideLength, y + middlePoint] = true;
                        }
                        if (!VertexPlacement[x + middlePoint, y + SideLength])
                        {
                            float averageRight = y + SideLength + middlePoint > Dimension - 1 ?
                            (Data[x, y + SideLength] + Data[x + SideLength, y + SideLength] + Data[x + middlePoint, y + middlePoint]) / 3 :
                            (Data[x, y + SideLength] + Data[x + SideLength, y + SideLength] + Data[x + middlePoint, y + middlePoint] +
                            Data[x + middlePoint, y + SideLength + middlePoint]) / 4;
                            averageRight += (float)rand.NextDouble() * 2 * RandomOffset - RandomOffset;

                            Data[x + middlePoint, y + SideLength] = averageRight;
                            VertexPlacement[x + middlePoint, y + SideLength] = true;
                        }
                    }
                }
            }
            if (Smoothing)
                for (int SideLength = Convert.ToInt32(Dimension - 1); SideLength >= 2; SideLength /= 2)
                {
                    int half = SideLength / 2;
                    for (int x = 0; x < Dimension; x += half)
                    {
                        for (int y = 0; y < Dimension; y += half)
                        {
                            int exposure = half > 64 ? 64 : half > 16 ? 10 : half > 8 ? 6 : half > 4 ? 2 : 1;
                            float Left = (y - exposure) < 0 ? Data[x, y] : Data[x, y - exposure];
                            float Right = (y + exposure) > (Dimension - exposure) ? Data[x, y] : Data[x, y + exposure];
                            float Bottom = (x + exposure) > (Dimension - exposure) ? Data[x, y] : Data[x + exposure, y];
                            float Top = (x - exposure) < 0 ? Data[x, y] : Data[x - exposure, y];
                            float average = (Left + Right + Top + Bottom) / 4;
                            if (Math.Pow(Math.Abs(average - Data[x, y]), 1.05) > 0.25f) Data[x, y] = average;
                        }
                    }
                }

        }

        vec3 GetSurfaceNormal(int x, int y)
        {
            vec3 ResultVector = new vec3(0.15f) * glm.normalize(new vec3(Data[x, y] - Data[x + 1, y], 1.0f, 0.0f)); //+x
            ResultVector += new vec3(0.15f) * glm.normalize(new vec3(Data[x - 1, y] - Data[x, y], 1.0f, 0.0f)); //-x
            ResultVector += new vec3(0.15f) * glm.normalize(new vec3(0.0f, 1.0f, Data[x, y] - Data[x, y + 1])); //+y
            ResultVector += new vec3(0.15f) * glm.normalize(new vec3(0.0f, 1.0f, Data[x, y - 1] - Data[x, y])); //-y

            float Sqrt = (float)Math.Sqrt(2.0);

            ResultVector += new vec3(0.1f) * glm.normalize(new vec3((Data[x, y] - Data[x + 1, y + 1]) / Sqrt, Sqrt, (Data[x, y] - Data[x + 1, y + 1]) / Sqrt)); //+x+y
            ResultVector += new vec3(0.1f) * glm.normalize(new vec3((Data[x, y] - Data[x + 1, y - 1]) / Sqrt, Sqrt, (Data[x + 1, y - 1] - Data[x, y]) / Sqrt)); //+x-y
            ResultVector += new vec3(0.1f) * glm.normalize(new vec3((Data[x - 1, y - 1] - Data[x, y]) / Sqrt, Sqrt, (Data[x - 1, y - 1] - Data[x, y]) / Sqrt)); //-x-y
            ResultVector += new vec3(0.1f) * glm.normalize(new vec3((Data[x - 1, y + 1] - Data[x, y]) / Sqrt, Sqrt, (Data[x, y] - Data[x - 1, y + 1]) / Sqrt)); //-x+y

            return glm.normalize(ResultVector);
        }

        struct Particle
        {
            public vec2 Pos;
            public vec2 Speed;

            public float Volume;
            public float Cargo;
            public Particle(vec2 pos)
            {
                Pos = pos;
                Speed = new vec2(0.0f);

                Volume = 1.0f;
                Cargo = 0.0f;


            }
            public Particle(float x, float y) : this(new vec2(x, y)) { }
            public Particle(int x, int y) : this(new vec2(x, y)) { }

        }

        void UpdatedErosion(uint cycles, int iter, bool parallel)
        {
            Parallel.For(0, parallel ? cycles : 1, r =>
            {
                for (uint i = 0; i < (parallel ? 1 : cycles); i++)
                {
                    Random Rand;
                    if (SeedIsUsed) Rand = new Random((int)(Seed + iter + i * 145 + r * 145));
                    else Rand = new Random();

                    float posX = (float)Rand.NextDouble() * (Dimension);
                    float posY = (float)Rand.NextDouble() * (Dimension);
                    vec2 ParticlePos = new vec2(posX, posY);

                    Particle Droplet = new Particle(ParticlePos);
                    Droplet.Volume = ParticleVolume;

                    while (Droplet.Volume > MinVolume)
                    {
                        int iPosX = (int)Droplet.Pos.x;
                        int iPosY = (int)Droplet.Pos.y;

                        if (iPosX <= 0 || iPosX >= (Dimension - 1) || iPosY <= 0 || iPosY >= (Dimension - 1)) break;

                        vec3 normal = GetSurfaceNormal(iPosX, iPosY);

                        Droplet.Speed += dt * new vec2(normal.x, normal.z) / (Droplet.Volume * Density);
                        Droplet.Speed *= (1.0f - dt * Friction);
                        Droplet.Pos += dt * Droplet.Speed;

                        if (Droplet.Pos.x <= 0.0f || Droplet.Pos.x >= (Dimension - 1) || Droplet.Pos.y <= 0.0f || Droplet.Pos.y >= (Dimension - 1)) break;

                        float MaxSediment = Droplet.Volume * MissingMath.Length(Droplet.Speed) * (Data[iPosX, iPosY] - Data[(int)Droplet.Pos.x, (int)Droplet.Pos.y]);
                        if (MaxSediment < 0.0f) MaxSediment = 0.0f;
                        float SedimentDifference = MaxSediment - Droplet.Cargo;

                        float val = dt * DepositionRate * SedimentDifference * Droplet.Volume;
                        Droplet.Cargo += val;
                        Data[iPosX, iPosY] -= val;

                        Droplet.Volume *= (1.0f - dt * EvaporationRate);
                    }
                }
            });
        }

        public float[,] GetArray()
        {
            return Data;
        }
        public uint GetDimension()
        {
            return Dimension;
        }
        public int GetHeight()
        {
            return Height;
        }
    }

}
