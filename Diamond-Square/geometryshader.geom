#version 400 core
layout (triangles) in;
layout (triangle_strip, max_vertices = 3) out;
out vec3 colour;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

uniform vec3 camera;

uniform bool LOD = false;
uniform float drawDistance;
uniform int dimension = 257;
uniform float height = 110;

float rand(vec2 co){
  return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

void main() {
	vec3 vector1 = gl_in[1].gl_Position.xyz - gl_in[0].gl_Position.xyz;
	vec3 vector2 = gl_in[2].gl_Position.xyz - gl_in[0].gl_Position.xyz;

	for(int i = 0; i < 3; i++) {
			
	vec3 positionCent = gl_in[i].gl_Position.xyz;
	float averageHeight = gl_in[i].gl_Position.z;//(gl_in[0].gl_Position.z + gl_in[1].gl_Position.z + gl_in[2].gl_Position.z) / 3;

	float temp = averageHeight / height;

	if(averageHeight > 0) {

		vec3 normal = normalize(cross(vector1, vector2));

		vec3 light = vec3(dimension / 2,dimension / 2,20000);

		float brightness = dot(normal, normalize(light - positionCent));
		if(brightness < 0) brightness = 0;

		float normal2 = pow(abs(brightness),2);
		
		
		float rand = rand(gl_in[i].gl_Position.xy) * 30;
		
		float topHeight = height - height / 6 + rand;

		if(averageHeight > topHeight) {
			colour = vec3(temp,temp, 1) * brightness;
		}

		if(averageHeight < topHeight && averageHeight > 0) {
			float temp32 = pow(temp, 32);
			float expo = 0;//pow(1/exp(temp), 4);
			
			colour = vec3(0.5 + 0.1 * temp32 + expo, 0.8  + 0.1 * temp32 + 0.5 * expo, pow(temp, 4)) * pow(brightness, 5);
		}
		if(normal2 < 0.7) {
			float normal3 = normal2 / 2.5;
			colour = vec3(normal3 / 1.2, normal3, normal3 * 1.1);
		}
	}
	if(averageHeight <= 0) {
		colour = vec3(-temp, -temp / 2, 0);
	}
	
	if(colour.x > 1) colour.x = 1;
	if(colour.y > 1) colour.y = 1;
	if(colour.z > 1) colour.z = 1;

	mat4 mvp = projection * view * model;

	gl_Position = mvp * gl_in[i].gl_Position;
	EmitVertex();

	//gl_Position = mvp * gl_in[1].gl_Position;
	//EmitVertex();

	//gl_Position = mvp * gl_in[2].gl_Position;
	//EmitVertex();
	}

	EndPrimitive();
}  
