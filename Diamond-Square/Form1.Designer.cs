﻿namespace Diamond_Square
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.openGLControl = new SharpGL.OpenGLControl();
            this.GeneratorButton = new System.Windows.Forms.Button();
            this.ControlsInfo = new System.Windows.Forms.TextBox();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.MapSizeTrackBar = new System.Windows.Forms.TrackBar();
            this.label1 = new System.Windows.Forms.Label();
            this.MapSizeInfo = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SeedLabel = new System.Windows.Forms.Label();
            this.SeedNumeric = new System.Windows.Forms.NumericUpDown();
            this.UseSeedCheckBox = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.EvapRateNum = new System.Windows.Forms.NumericUpDown();
            this.ParticleVolumeNum = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.MinVolumeNum = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.DensityNum = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.FrictionNum = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.DepositionRateNum = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.TimeStepNum = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.ParticlePerCycleNum = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.ErosionIterationsNum = new System.Windows.Forms.NumericUpDown();
            this.ExportButton = new System.Windows.Forms.Button();
            this.backgroundWorkerExport = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.ParallelErosionCheck = new System.Windows.Forms.CheckBox();
            this.SmoothingCheck = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.openGLControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MapSizeTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SeedNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EvapRateNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ParticleVolumeNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinVolumeNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DensityNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrictionNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DepositionRateNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimeStepNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ParticlePerCycleNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErosionIterationsNum)).BeginInit();
            this.SuspendLayout();
            // 
            // openGLControl
            // 
            this.openGLControl.DrawFPS = false;
            this.openGLControl.FrameRate = 180;
            this.openGLControl.Location = new System.Drawing.Point(12, 12);
            this.openGLControl.Name = "openGLControl";
            this.openGLControl.OpenGLVersion = SharpGL.Version.OpenGLVersion.OpenGL4_0;
            this.openGLControl.RenderContextType = SharpGL.RenderContextType.FBO;
            this.openGLControl.RenderTrigger = SharpGL.RenderTrigger.TimerBased;
            this.openGLControl.Size = new System.Drawing.Size(961, 515);
            this.openGLControl.TabIndex = 0;
            this.openGLControl.OpenGLDraw += new SharpGL.RenderEventHandler(this.OpenGLDraw);
            this.openGLControl.Load += new System.EventHandler(this.openGLControl_Load);
            this.openGLControl.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyboardPress);
            // 
            // GeneratorButton
            // 
            this.GeneratorButton.Location = new System.Drawing.Point(800, 533);
            this.GeneratorButton.Name = "GeneratorButton";
            this.GeneratorButton.Size = new System.Drawing.Size(172, 35);
            this.GeneratorButton.TabIndex = 1;
            this.GeneratorButton.Text = "Generate";
            this.GeneratorButton.UseVisualStyleBackColor = true;
            this.GeneratorButton.Click += new System.EventHandler(this.GeneratorButton_Click);
            // 
            // ControlsInfo
            // 
            this.ControlsInfo.Location = new System.Drawing.Point(11, 533);
            this.ControlsInfo.Multiline = true;
            this.ControlsInfo.Name = "ControlsInfo";
            this.ControlsInfo.ReadOnly = true;
            this.ControlsInfo.Size = new System.Drawing.Size(385, 35);
            this.ControlsInfo.TabIndex = 5;
            this.ControlsInfo.Text = "Use W, A, S, D to move horizontaly; SPACE, C to move vertically\r\nUse I, K, J, L t" +
    "o rotate the camera; Press Generate button to create new terrain";
            this.ControlsInfo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.ControlsInfo.Enter += new System.EventHandler(this.ControlsInfo_Enter);
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(402, 533);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(392, 35);
            this.progressBar.TabIndex = 6;
            // 
            // MapSizeTrackBar
            // 
            this.MapSizeTrackBar.LargeChange = 1;
            this.MapSizeTrackBar.Location = new System.Drawing.Point(975, 30);
            this.MapSizeTrackBar.Maximum = 13;
            this.MapSizeTrackBar.Minimum = 7;
            this.MapSizeTrackBar.Name = "MapSizeTrackBar";
            this.MapSizeTrackBar.Size = new System.Drawing.Size(79, 45);
            this.MapSizeTrackBar.TabIndex = 8;
            this.MapSizeTrackBar.Value = 7;
            this.MapSizeTrackBar.ValueChanged += new System.EventHandler(this.MapSizeTrackBar_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(976, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Map Size (129-8193)";
            // 
            // MapSizeInfo
            // 
            this.MapSizeInfo.AutoSize = true;
            this.MapSizeInfo.Location = new System.Drawing.Point(1052, 30);
            this.MapSizeInfo.Name = "MapSizeInfo";
            this.MapSizeInfo.Size = new System.Drawing.Size(25, 13);
            this.MapSizeInfo.TabIndex = 10;
            this.MapSizeInfo.Text = "129";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(976, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Erosion iterations";
            // 
            // SeedLabel
            // 
            this.SeedLabel.AutoSize = true;
            this.SeedLabel.Enabled = false;
            this.SeedLabel.Location = new System.Drawing.Point(976, 125);
            this.SeedLabel.Name = "SeedLabel";
            this.SeedLabel.Size = new System.Drawing.Size(32, 13);
            this.SeedLabel.TabIndex = 15;
            this.SeedLabel.Text = "Seed";
            // 
            // SeedNumeric
            // 
            this.SeedNumeric.Enabled = false;
            this.SeedNumeric.Location = new System.Drawing.Point(979, 141);
            this.SeedNumeric.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.SeedNumeric.Name = "SeedNumeric";
            this.SeedNumeric.Size = new System.Drawing.Size(100, 20);
            this.SeedNumeric.TabIndex = 16;
            this.SeedNumeric.ValueChanged += new System.EventHandler(this.SeedNumeric_ValueChanged);
            // 
            // UseSeedCheckBox
            // 
            this.UseSeedCheckBox.AutoSize = true;
            this.UseSeedCheckBox.Location = new System.Drawing.Point(979, 105);
            this.UseSeedCheckBox.Name = "UseSeedCheckBox";
            this.UseSeedCheckBox.Size = new System.Drawing.Size(71, 17);
            this.UseSeedCheckBox.TabIndex = 17;
            this.UseSeedCheckBox.Text = "Use seed";
            this.UseSeedCheckBox.UseVisualStyleBackColor = true;
            this.UseSeedCheckBox.CheckedChanged += new System.EventHandler(this.UseSeedCheckBox_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(976, 165);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "Evaporation rate";
            // 
            // EvapRateNum
            // 
            this.EvapRateNum.DecimalPlaces = 5;
            this.EvapRateNum.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.EvapRateNum.Location = new System.Drawing.Point(979, 186);
            this.EvapRateNum.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.EvapRateNum.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            327680});
            this.EvapRateNum.Name = "EvapRateNum";
            this.EvapRateNum.Size = new System.Drawing.Size(100, 20);
            this.EvapRateNum.TabIndex = 19;
            this.EvapRateNum.Value = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.EvapRateNum.ValueChanged += new System.EventHandler(this.EvapRateNum_ValueChanged);
            // 
            // ParticleVolumeNum
            // 
            this.ParticleVolumeNum.DecimalPlaces = 5;
            this.ParticleVolumeNum.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.ParticleVolumeNum.Location = new System.Drawing.Point(979, 225);
            this.ParticleVolumeNum.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.ParticleVolumeNum.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            327680});
            this.ParticleVolumeNum.Name = "ParticleVolumeNum";
            this.ParticleVolumeNum.Size = new System.Drawing.Size(100, 20);
            this.ParticleVolumeNum.TabIndex = 21;
            this.ParticleVolumeNum.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.ParticleVolumeNum.ValueChanged += new System.EventHandler(this.ParticleVolumeNum_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(976, 209);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 20;
            this.label4.Text = "Volume";
            // 
            // MinVolumeNum
            // 
            this.MinVolumeNum.DecimalPlaces = 5;
            this.MinVolumeNum.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.MinVolumeNum.Location = new System.Drawing.Point(979, 264);
            this.MinVolumeNum.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.MinVolumeNum.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            327680});
            this.MinVolumeNum.Name = "MinVolumeNum";
            this.MinVolumeNum.Size = new System.Drawing.Size(100, 20);
            this.MinVolumeNum.TabIndex = 23;
            this.MinVolumeNum.Value = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.MinVolumeNum.ValueChanged += new System.EventHandler(this.MinVolumeNum_ValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(976, 248);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 22;
            this.label5.Text = "Min volume";
            // 
            // DensityNum
            // 
            this.DensityNum.DecimalPlaces = 5;
            this.DensityNum.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.DensityNum.Location = new System.Drawing.Point(979, 303);
            this.DensityNum.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.DensityNum.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            327680});
            this.DensityNum.Name = "DensityNum";
            this.DensityNum.Size = new System.Drawing.Size(100, 20);
            this.DensityNum.TabIndex = 25;
            this.DensityNum.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.DensityNum.ValueChanged += new System.EventHandler(this.DensityNum_ValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(976, 287);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 13);
            this.label6.TabIndex = 24;
            this.label6.Text = "Density";
            // 
            // FrictionNum
            // 
            this.FrictionNum.DecimalPlaces = 5;
            this.FrictionNum.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.FrictionNum.Location = new System.Drawing.Point(979, 342);
            this.FrictionNum.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.FrictionNum.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            327680});
            this.FrictionNum.Name = "FrictionNum";
            this.FrictionNum.Size = new System.Drawing.Size(100, 20);
            this.FrictionNum.TabIndex = 27;
            this.FrictionNum.Value = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.FrictionNum.ValueChanged += new System.EventHandler(this.FrictionNum_ValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(977, 326);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 13);
            this.label7.TabIndex = 26;
            this.label7.Text = "Friction";
            // 
            // DepositionRateNum
            // 
            this.DepositionRateNum.DecimalPlaces = 5;
            this.DepositionRateNum.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.DepositionRateNum.Location = new System.Drawing.Point(980, 381);
            this.DepositionRateNum.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.DepositionRateNum.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            327680});
            this.DepositionRateNum.Name = "DepositionRateNum";
            this.DepositionRateNum.Size = new System.Drawing.Size(100, 20);
            this.DepositionRateNum.TabIndex = 29;
            this.DepositionRateNum.Value = new decimal(new int[] {
            2,
            0,
            0,
            65536});
            this.DepositionRateNum.ValueChanged += new System.EventHandler(this.DepositionRateNum_ValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(976, 365);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(78, 13);
            this.label8.TabIndex = 28;
            this.label8.Text = "Deposition rate";
            // 
            // TimeStepNum
            // 
            this.TimeStepNum.DecimalPlaces = 5;
            this.TimeStepNum.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.TimeStepNum.Location = new System.Drawing.Point(979, 420);
            this.TimeStepNum.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.TimeStepNum.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            327680});
            this.TimeStepNum.Name = "TimeStepNum";
            this.TimeStepNum.Size = new System.Drawing.Size(100, 20);
            this.TimeStepNum.TabIndex = 31;
            this.TimeStepNum.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.TimeStepNum.ValueChanged += new System.EventHandler(this.TimeStepNum_ValueChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(976, 404);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 13);
            this.label9.TabIndex = 30;
            this.label9.Text = "Time step";
            // 
            // ParticlePerCycleNum
            // 
            this.ParticlePerCycleNum.Location = new System.Drawing.Point(979, 459);
            this.ParticlePerCycleNum.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.ParticlePerCycleNum.Name = "ParticlePerCycleNum";
            this.ParticlePerCycleNum.Size = new System.Drawing.Size(100, 20);
            this.ParticlePerCycleNum.TabIndex = 33;
            this.ParticlePerCycleNum.Value = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.ParticlePerCycleNum.ValueChanged += new System.EventHandler(this.ParticlePerCycleNum_ValueChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(976, 443);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(90, 13);
            this.label10.TabIndex = 32;
            this.label10.Text = "Particles/Iteration";
            // 
            // ErosionIterationsNum
            // 
            this.ErosionIterationsNum.Location = new System.Drawing.Point(979, 79);
            this.ErosionIterationsNum.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.ErosionIterationsNum.Name = "ErosionIterationsNum";
            this.ErosionIterationsNum.Size = new System.Drawing.Size(98, 20);
            this.ErosionIterationsNum.TabIndex = 34;
            this.ErosionIterationsNum.ValueChanged += new System.EventHandler(this.ErosionIterationsNum_ValueChanged);
            // 
            // ExportButton
            // 
            this.ExportButton.Location = new System.Drawing.Point(978, 532);
            this.ExportButton.Name = "ExportButton";
            this.ExportButton.Size = new System.Drawing.Size(102, 35);
            this.ExportButton.TabIndex = 35;
            this.ExportButton.Text = "Export as .obj";
            this.ExportButton.UseVisualStyleBackColor = true;
            this.ExportButton.Click += new System.EventHandler(this.ExportButton_Click);
            // 
            // backgroundWorkerExport
            // 
            this.backgroundWorkerExport.WorkerReportsProgress = true;
            this.backgroundWorkerExport.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerExport_DoWork);
            this.backgroundWorkerExport.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker1_ProgressChanged);
            this.backgroundWorkerExport.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerExport_RunWorkerCompleted);
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker1_ProgressChanged);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // ParallelErosionCheck
            // 
            this.ParallelErosionCheck.AutoSize = true;
            this.ParallelErosionCheck.Location = new System.Drawing.Point(980, 483);
            this.ParallelErosionCheck.Name = "ParallelErosionCheck";
            this.ParallelErosionCheck.Size = new System.Drawing.Size(98, 17);
            this.ParallelErosionCheck.TabIndex = 36;
            this.ParallelErosionCheck.Text = "Parallel Erosion";
            this.ParallelErosionCheck.UseVisualStyleBackColor = true;
            this.ParallelErosionCheck.CheckedChanged += new System.EventHandler(this.ParallelErosionCheck_CheckedChanged);
            // 
            // SmoothingCheck
            // 
            this.SmoothingCheck.AutoSize = true;
            this.SmoothingCheck.Checked = true;
            this.SmoothingCheck.CheckState = System.Windows.Forms.CheckState.Checked;
            this.SmoothingCheck.Location = new System.Drawing.Point(980, 506);
            this.SmoothingCheck.Name = "SmoothingCheck";
            this.SmoothingCheck.Size = new System.Drawing.Size(76, 17);
            this.SmoothingCheck.TabIndex = 37;
            this.SmoothingCheck.Text = "Smoothing";
            this.SmoothingCheck.UseVisualStyleBackColor = true;
            this.SmoothingCheck.CheckedChanged += new System.EventHandler(this.SmoothingCheck_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1084, 579);
            this.Controls.Add(this.SmoothingCheck);
            this.Controls.Add(this.ParallelErosionCheck);
            this.Controls.Add(this.ExportButton);
            this.Controls.Add(this.ErosionIterationsNum);
            this.Controls.Add(this.ParticlePerCycleNum);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.TimeStepNum);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.DepositionRateNum);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.FrictionNum);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.DensityNum);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.MinVolumeNum);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.ParticleVolumeNum);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.EvapRateNum);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.UseSeedCheckBox);
            this.Controls.Add(this.SeedNumeric);
            this.Controls.Add(this.SeedLabel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.MapSizeInfo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.MapSizeTrackBar);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.ControlsInfo);
            this.Controls.Add(this.GeneratorButton);
            this.Controls.Add(this.openGLControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Enter += new System.EventHandler(this.ControlsInfo_Enter);
            ((System.ComponentModel.ISupportInitialize)(this.openGLControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MapSizeTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SeedNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EvapRateNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ParticleVolumeNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinVolumeNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DensityNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrictionNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DepositionRateNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimeStepNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ParticlePerCycleNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErosionIterationsNum)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private SharpGL.OpenGLControl openGLControl;
        private System.Windows.Forms.Button GeneratorButton;
        private System.Windows.Forms.TextBox ControlsInfo;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.TrackBar MapSizeTrackBar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label MapSizeInfo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label SeedLabel;
        private System.Windows.Forms.NumericUpDown SeedNumeric;
        private System.Windows.Forms.CheckBox UseSeedCheckBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown EvapRateNum;
        private System.Windows.Forms.NumericUpDown ParticleVolumeNum;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown MinVolumeNum;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown DensityNum;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown FrictionNum;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown DepositionRateNum;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown TimeStepNum;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown ParticlePerCycleNum;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown ErosionIterationsNum;
        private System.Windows.Forms.Button ExportButton;
        private System.ComponentModel.BackgroundWorker backgroundWorkerExport;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.CheckBox ParallelErosionCheck;
        private System.Windows.Forms.CheckBox SmoothingCheck;
    }
}

