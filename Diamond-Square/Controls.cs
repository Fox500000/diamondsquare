﻿using GlmNet;
using System.Windows.Forms;

namespace Diamond_Square
{
    public class Controls
    {
        private vec3 _CameraPos;
        private vec3 _LookVector;
        private vec3 _InertiaVector;
        private vec3 _VelocityVector;

        float MovementSpeed = 10;

        float AngleHorizontal = 0;
        float AngleZ = 0;

        public vec3 CameraPos { get => _CameraPos; }
        public vec3 LookVector { get => _LookVector; }
        public vec3 InertiaVector { get => _InertiaVector; }
        public vec3 VelocityVector { get => _VelocityVector; }

        public Controls() {
            _CameraPos = new vec3(5,2,5);
            _LookVector = new vec3(glm.sin(AngleHorizontal),glm.cos(AngleHorizontal), glm.sin(AngleZ));
            _InertiaVector = new vec3(0);
            _VelocityVector = new vec3(0);
        }
        public void KeyboardPressHandle(object sender, KeyPressEventArgs e)
        {
            float Pi2 = 1.57f;

            if ((char)e.KeyChar == 'l')
            {
                AngleHorizontal += 0.1f;
            }
            if ((char)e.KeyChar == 'j')
            {
                AngleHorizontal -= 0.1f;
            }

            if ((char)e.KeyChar == 'i')
            {
                AngleZ += Pi2 / 10;
            }
            if ((char)e.KeyChar == 'k')
            {
                AngleZ -= Pi2 / 10;
            }

            if (AngleHorizontal >= Pi2 * 4) AngleHorizontal = 0;
            if (AngleHorizontal < 0) AngleHorizontal = Pi2 * 4 + AngleHorizontal;
            if (AngleZ >= Pi2) AngleZ = Pi2;
            if (AngleZ <= -Pi2) AngleZ = -Pi2;
            
            _LookVector.x = glm.sin(AngleHorizontal);
            _LookVector.y = glm.cos(AngleHorizontal);
            _LookVector.z = glm.sin(AngleZ);
            _LookVector = glm.normalize(_LookVector);
            //_LookVector = new vec3(glm.sin(AngleHorizontal), glm.cos(AngleHorizontal), glm.sin(AngleZ));
            if ((char)e.KeyChar == ' ') _CameraPos.z += MovementSpeed;
            if ((char)e.KeyChar == 'c') _CameraPos.z -= MovementSpeed;
            if ((char)e.KeyChar == 'w')
            {
                _CameraPos.x += _LookVector.x * MovementSpeed;
                _CameraPos.y += _LookVector.y * MovementSpeed;
            }
            if ((char)e.KeyChar == 's')
            {
                _CameraPos.y -= _LookVector.y * MovementSpeed;
                _CameraPos.x -= _LookVector.x * MovementSpeed;
            }
            if ((char)e.KeyChar == 'a')
            {
                vec3 TempLookVector = new vec3(glm.sin(AngleHorizontal - Pi2), glm.cos(AngleHorizontal - Pi2), glm.sin(AngleZ));
                _CameraPos.x += glm.normalize(TempLookVector).x * MovementSpeed;
                _CameraPos.y += glm.normalize(TempLookVector).y * MovementSpeed;
            }
            if ((char)e.KeyChar == 'd')
            {
                vec3 TempLookVector = new vec3(glm.sin(AngleHorizontal + Pi2), glm.cos(AngleHorizontal + Pi2), glm.sin(AngleZ));
                _CameraPos.x += glm.normalize(TempLookVector).x * MovementSpeed;
                _CameraPos.y += glm.normalize(TempLookVector).y * MovementSpeed;
            }
        }
    }
}
