﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GlmNet;
using System.ComponentModel;

namespace Diamond_Square
{
    public class OBJExport
    {
        public OBJExport() { }

        public bool Export(float[,] data, uint dim, BackgroundWorker worker)
        {
            //Data is taken from algorithm
            try
            {

                var Writer = new StreamWriter("landscape.obj", false);
                worker.ReportProgress(0);
                for (uint i = 0; i < dim; i++)
                {
                    for (uint k = 0; k < dim; k++)
                    {
                        Writer.WriteLine(String.Format("v {0} {1} {2} {3}", (float)i * 1000 / dim, data[i, k], (float)k * 1000 / dim, 1.0f).Replace(",", "."));
                    }
                }
                worker.ReportProgress(25);
                for (uint i = 0; i < dim; i++)
                {
                    for (uint k = 0; k < dim; k++)
                    {
                        Writer.WriteLine(String.Format("vt {0} {1} {2}", (float)i / dim, (float)k / dim, 0).Replace(",", "."));
                    }
                }
                worker.ReportProgress(50);

                for (uint i = 0; i < dim; i++)
                {
                    for (uint k = 0; k < dim; k++)
                    {
                        if (i == dim - 1 || k == dim - 1)
                        {
                            Writer.WriteLine(String.Format("vn {0} {1} {2}", 0.0f, 1.0f, 0.0f).Replace(",", "."));
                            break;
                        }
                        vec3 FirstVec = new vec3(1.0f, data[i + 1, k] - data[i, k], 0.0f);
                        vec3 SecondVec = new vec3(0.0f, data[i, k + 1] - data[i, k], 1.0f);
                        vec3 Result = glm.normalize(glm.cross(SecondVec, FirstVec));
                        Writer.WriteLine(String.Format("vn {0} {1} {2}", (float)Result.x, (float)Result.y, (float)Result.z).Replace(",", "."));
                    }
                }
                worker.ReportProgress(75);

                for (uint i = 1; i < dim; i++)
                {
                    for (uint k = 1; k < dim; k++)
                    {
                        Writer.WriteLine(String.Format("f {0} {1} {2}", i * dim + k, (i + 1) * dim + k + 1, (i + 1) * dim + k).Replace(",", "."));
                        Writer.WriteLine(String.Format("f {0} {1} {2}", i * dim + k, i * dim + k + 1, (i + 1) * dim + k + 1).Replace(",", "."));
                    }
                }
                Writer.Close();
                worker.ReportProgress(100);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}
