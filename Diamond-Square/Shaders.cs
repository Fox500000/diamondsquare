﻿using System;
using System.IO;
using System.Text;
using SharpGL;
using SharpGL.Shaders;

namespace Diamond_Square
{
    class Shader
    {
        public uint ID { get; } = 0;
        private uint vertexShader = 0;
        private uint geometryShader = 0;
        private uint fragmentShader = 0;

        public Shader(string vertexShaderSourceFile, string fragmentShaderSourceFile, OpenGL GL, string geometryShaderSourceFile = "")
        {
            string vertexCode = "";
            string geometryCode = "";
            string fragmentCode = "";
            try
            {
                vertexCode = string.Join("\n", File.ReadAllLines(vertexShaderSourceFile));
                if(geometryShaderSourceFile != "") geometryCode = string.Join("\n", File.ReadAllLines(geometryShaderSourceFile));
                fragmentCode = string.Join("\n", File.ReadAllLines(fragmentShaderSourceFile));
            }
            catch (FileNotFoundException e)
            {
                throw e;
            }

            vertexShader = GL.CreateShader(OpenGL.GL_VERTEX_SHADER); // create
            GL.ShaderSource(vertexShader, vertexCode); // link source
            GL.CompileShader(vertexShader); // compile

            if (GetCompileStatus(GL, vertexShader) == false)
            {
                throw new ShaderCompilationException(string.Format("Failed to compile shader with ID {0}.", vertexShader), GetInfoLog(GL, vertexShader));
            }
            if (geometryShaderSourceFile != "")
            {
                geometryShader = GL.CreateShader(OpenGL.GL_GEOMETRY_SHADER); // create
                GL.ShaderSource(geometryShader, geometryCode); // link source
                GL.CompileShader(geometryShader); // compile

                if (GetCompileStatus(GL, geometryShader) == false)
                {
                    throw new ShaderCompilationException(string.Format("Failed to compile shader with ID {0}.", geometryShader), GetInfoLog(GL, geometryShader));
                }
            }
            fragmentShader = GL.CreateShader(OpenGL.GL_FRAGMENT_SHADER); // create
            GL.ShaderSource(fragmentShader, fragmentCode); // link source
            GL.CompileShader(fragmentShader); // compile

            if (GetCompileStatus(GL, fragmentShader) == false)
            {
                throw new ShaderCompilationException(string.Format("Failed to compile shader with ID {0}.", fragmentShader), GetInfoLog(GL, fragmentShader));
            }

            ID = GL.CreateProgram();
            GL.AttachShader(ID, vertexShader);
            if (geometryShaderSourceFile != "") GL.AttachShader(ID, geometryShader);
            GL.AttachShader(ID, fragmentShader);
            GL.BindAttribLocation(ID, 0, "vertexPosition");
            GL.LinkProgram(ID);

            if (GetLinkStatus(GL, ID) == false)
            {
                throw new ShaderCompilationException(string.Format("Failed to link program with ID {0}.", ID), GetProgramInfoLog(GL, ID));
            }
            GL.DeleteShader(vertexShader);
            if (geometryShaderSourceFile != "") GL.DeleteShader(geometryShader);
            GL.DeleteShader(fragmentShader);
        }

        private bool GetCompileStatus(OpenGL gl, uint shader)
        {
            int[] parameters = new int[] { 0 };
            gl.GetShader(shader, OpenGL.GL_COMPILE_STATUS, parameters);
            return parameters[0] == OpenGL.GL_TRUE;
        }

        private bool GetLinkStatus(OpenGL gl, uint id)
        {
            int[] parameters = new int[] { 0 };
            gl.GetProgram(id, OpenGL.GL_LINK_STATUS, parameters);
            return parameters[0] == OpenGL.GL_TRUE;
        }

        private string GetInfoLog(OpenGL gl, uint shader)
        {
            //  Get the info log length.
            int[] infoLength = new int[] { 0 };
            gl.GetShader(shader, OpenGL.GL_INFO_LOG_LENGTH, infoLength);
            int bufSize = infoLength[0];

            //  Get the compile info.
            StringBuilder il = new StringBuilder(bufSize);
            gl.GetShaderInfoLog(shader, bufSize, IntPtr.Zero, il);

            return il.ToString();
        }

        private string GetProgramInfoLog(OpenGL gl, uint id)
        {
            //  Get the info log length.
            int[] infoLength = new int[] { 0 };
            gl.GetProgram(id, OpenGL.GL_INFO_LOG_LENGTH, infoLength);
            int bufSize = infoLength[0];

            //  Get the compile info.
            StringBuilder il = new StringBuilder(bufSize);
            gl.GetProgramInfoLog(id, bufSize, IntPtr.Zero, il);

            return il.ToString();
        }
        

    }


}
