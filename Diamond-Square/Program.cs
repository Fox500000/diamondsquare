﻿using System;
using System.Windows.Forms;

namespace Diamond_Square
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Controls ControlsHandler = new Controls();
            Algorithm AlgoritmHandler = new Algorithm();
            OBJExport Exporter = new OBJExport();

            Application.Run(new Form1(ControlsHandler, AlgoritmHandler, Exporter));
        }
    }
}
